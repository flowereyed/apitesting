﻿using NUnit.Framework;
using RestSharp;
using System;
using System.Net;

namespace apitesting
{
    [TestFixture]
    public class RestSharpTest
    {
        private static string url = "http://api.openweathermap.org/data/2.5/forecast?id=524901&APPID=4d390793adf3b0458e544e9723bc4dfc";
        private const string CONTENT_TYPE = "application/json; charset=utf-8";
        static IRestResponse response;

        [SetUp]
        public static void InitTest()
        {
            IRestClient client = new RestClient();
            IRestRequest request = new RestRequest(url);
            response = client.Get(request);
        }

        [Test]
        public void CheckStatusCodeTest()
        {
            HttpStatusCode statusCode = response.StatusCode;
            if (response.IsSuccessful)
            {
                Console.WriteLine("Status Code: " + statusCode);
                Assert.AreEqual(statusCode, HttpStatusCode.OK);
            }
        }

        [Test]
        public void CheckContentTypeTest()
        {
            if (response.IsSuccessful)
            {
                Assert.AreEqual(response.ContentType, CONTENT_TYPE);
            }
        }

        [Test]
        public void CheckContentTest()
        {
            string content = response.Content;
            if (response.IsSuccessful)
            {
                Console.WriteLine("Content: " + content);
                Assert.IsTrue(content.Contains("weather"));
            }
        }
    }
}